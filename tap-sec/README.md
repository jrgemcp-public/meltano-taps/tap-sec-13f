# tap-sec

`tap-sec` is a Singer tap for tap-sec.

Built with the [Meltano Tap SDK](https://sdk.meltano.com) for Singer Taps.

## Usage

You can easily run `tap-sec` by itself or in a pipeline using [Meltano](https://meltano.com/).

### Executing the Tap Directly

```bash
meltano install
meltano run tap-sec target-parquet
```

### References

* https://github.com/CodeWritingCow/sec-web-scraper-13f